## Signal

Signal, Whatsapp'a alternatif olarak çıkmış merkezi mesajlaşma uygulamasıdır. Eskiden olsa normal şartlarda telefon numarama sahip kişiler benimle iletişime geçebilirdi fakat kullanıcı ile de iletişime geçilebiliyor. Sizde telefon numaram varsa iletişime geçebiliriz. Yoksa da [bu](https://signal.me/#eu/L-xlfbC53v6v4zZ5IBmhh8LXtnfxSyrX4FLaMdUS1iR06GTSlA1E8KJZYKCl_rEf) bağlantı üzerinden telefon numaranızı vermeden Signal üzerinden mesajlaşmamız mümkün.

## Matrix

Matrix, federasyon mantığıyla çalışan anlık mesajlaşma uygulamasıdır, varsayılan olarak uçtan uca şifrelemeyi destekler.

Matrix adresim: [@erenkaplan:matrix.org](https://matrix.to/#/@erenkaplan:matrix.org)

## E-posta

E-posta atarken GPG kullanmaya özen gösterin. GPG adresime [şuradan](/gpg/gpg.asc) ulaşabilirsiniz. E-posta gönderebileceğiniz adresler:

- {{< cloakemail address="erenkaplan@tutanota.com" >}}
- {{< cloakemail address="erenkaplan@disroot.org" >}}
- {{< cloakemail address="erenkaplan@protonmail.com" >}}