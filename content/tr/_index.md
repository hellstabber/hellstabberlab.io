Selamlar, Eren ben.

Uzun süredir blog yazmaya hevesleniyorum, sonradan da unutup gidiyorum. Kafamı kurcalayan şeyleri genelde bir yerlere yazarım. Burayı da yazdığım şeylerin insanlara ulaşması için ve iletişim adreslerimin derli toplu bir yerde durması için kullanacağım.

Burada:

- İşletim sistemleri, özellikle GNU/Linux hakkında yazı bulabilirsiniz. Alaylı ve okullu olarak kendimi geliştirmek istediğim bir alan.
- Okuduğum, düşündüğüm şeyler hakkında,
- Amatör koşu ve parkour sporları hakkında,
- Ve genel olarak ruh halimi ifade ettiğim yazılar bulabilirsin.

Sosyal Mecralar:

- [mastodon](https://defcon.social/@hellstabber)
- [ex-twitter](https://twitter.com/helstabber)
- [last.fm](https://www.last.fm/user/hellstabber)
- [backloggd](https://www.backloggd.com/u/hellstabber/)
- [goodreads](https://goodreads.com/hellstabber)

---

[![https://defcon.social/@hellstabber](/images/88x31/fediverse.gif)](https://defcon.social/@hellstabber) [![](/images/88x31/pgp-now.gif)](/gpg/gpg.asc) ![](/images/88x31/gnubanner.gif) ![](/images/88x31/gplv3.gif) ![](/images/88x31/debian.webp) ![](/images/88x31/gnu-linux.gif) ![miv403'e teşekkürlerimi sunuyorum](/images/88x31/openbased.png) 

![](/images/88x31/lain.webp) ![](/images/88x31/lain2.webp) ![](/images/88x31/lain3.gif) ![](/images/88x31/evangelion.gif) ![](/images/88x31/volta.gif) ![](/images/88x31/diablo2.gif) ![](/images/88x31/half-life.gif) 

![](/images/88x31/sucks.gif) ![](/images/88x31/emacs2.gif) ![](/images/88x31/firefox2.gif)  [![mailto:erenkaplan@tutanota.com](/images/88x31/email.gif)](mailto:tutanota@tutanota.com) ![](/images/88x31/fishtank.webp)

[![](/images/insan-yazimi.png)](https://notbyai.fyi/)

<a rel="me" href="https://defcon.social/@hellstabber"></a>